## Содержимое Dockerfile

```

FROM debian:9 as build

RUN apt-get update && apt-get install -y wget gcc make libnginx-mod-http-lua
RUN apt install -y libpcre3 libpcre3-dev zlib1g zlib1g-dev libssl-dev
RUN export LUAJIT_LIB=/path/to/luajit/lib
RUN export LUAJIT_INC=/path/to/luajit/include/luajit-2.0
RUN export LUAJIT_LIB=/path/to/luajit/lib
RUN export LUAJIT_INC=/path/to/luajit/include/luajit-2.1
RUN wget 'https://openresty.org/download/nginx-1.19.3.tar.gz' && tar -xzvf nginx-1.19.3.tar.gz && cd nginx-1.19.3/>


FROM debian:9
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]

```


## Для 100 процентов добавил необходимые пакеты и главное запись в конфигурацию NGINX в самое начало

```
load_module /usr/lib/nginx/modules/ndk_http_module.so;
load_module /usr/lib/nginx/modules/ngx_http_lua_module.so;
pcre_jit on;
```

